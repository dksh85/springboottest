package com.example.demo.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.sample.mapper.SampleMapper;

@Service
public class SampleService {
	
	@Autowired
	private SampleMapper sampleMapper;
	
	public String selectTest() {
		return sampleMapper.selectTest();
	}

}