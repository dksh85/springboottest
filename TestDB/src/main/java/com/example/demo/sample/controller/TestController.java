package com.example.demo.sample.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@GetMapping(value="/Hello2/{variable}")
	public String hello(@PathVariable String variable) {
		return variable;
	}
	
	@GetMapping(value="/Hi/{variable2}")
	public String hello2(@PathVariable("variable2") String var) {
		return var;
	}
	
	// http://localhost:3080/mapRequest?key1=value1&key2=value2  --> 이경우 iteration은 두번
	@GetMapping(value="/mapRequest")
	public String getRequestParam2(@RequestParam Map<String, String> param) {
		StringBuilder sb = new StringBuilder();
		param.entrySet().forEach(pam->{
			sb.append(pam.getKey() + " : " + pam.getValue() + "\n");
		});
		return sb.toString();
	}
}
