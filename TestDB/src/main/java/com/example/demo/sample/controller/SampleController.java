package com.example.demo.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.sample.service.SampleService;

@RestController
public class SampleController {
	
	@Autowired
	private SampleService sampleService;
	
	@RequestMapping(value="/index", method = {RequestMethod.POST, RequestMethod.GET})
	public String index() {
		System.out.println("index page call");
		String test = sampleService.selectTest();
		System.out.println("test : "+test);
		return "index";
	}
	
	@RequestMapping("/hello")
	public String hello() {
		return "Hello world";
	}

}